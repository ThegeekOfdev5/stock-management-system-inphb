<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Etudiant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class EtudiantController extends Controller
{
    //
    public function studentAdd()
    {
        return view('users.student.add');
    }

    public function studentList()
    {
        $etudiants = Etudiant::paginate(8);
        return view('users.student.list', compact('etudiants'));
    }

    public function editRecord($id)
    {
        $etudiant = Etudiant::where('id',$id)->first();
        return view('users.student.edit',compact('etudiant'));
    }

    public function saveStudent(Request $request)
    {
        $request->validate([
            'nom'           => 'required|string|max:255',
            'prenom'        => 'required|string|max:255',
            'matricule'     => 'required|string|max:255',
            'ecole'         => 'required|string|max:255',
            'parcours'      => 'required|string|max:255',
            'filiere'       => 'required|string|max:255',
            'classe'        => 'required|string|max:255',
            'email'         => 'required|string|email|max:255',
            'contacts'      => 'required|string|max:255',
            'upload_image'  => 'required|image|file|mimes:jpg,jpeg,png'
        ]);
        try {
            $upload_profile = rand() . '.' . $request->upload_image->extension();
            $request->upload_image->move(public_path('/images/avatar/'), $upload_profile);
            if(!empty($upload_profile)) {

                $dt        = Carbon::now();
                $todayDate = $dt->toDayDateTimeString();


                User::create([
                    'username'  => $request->nom."@".$request->matricule,
                    'email'     => $request->email,
                    'join_date' => $todayDate,
                    'role'      => 'Etudiant',
                    'status'    => 'Active',
                    'contacts'  => $request->contacts,
                    'avatar'    => $upload_profile,
                    'password'  => Hash::make($request->matricule),
                ]);
                $user_id = DB::table('users')->select('user_id')->orderBy('id','DESC')->first();

                $saveRecord = new Etudiant();
                $saveRecord->user_id          = $user_id->user_id;
                $saveRecord->nom              = $request->nom;
                $saveRecord->prenom           = $request->prenom;
                $saveRecord->matricule        = $request->matricule;
                $saveRecord->ecole            = $request->ecole;
                $saveRecord->parcours         = $request->parcours;
                $saveRecord->filiere          = $request->filiere;
                $saveRecord->classe           = $request->classe;
                $saveRecord->email            = $request->email;
                $saveRecord->contacts         = $request->contacts;
                $saveRecord->upload_image     = $upload_profile;
                $saveRecord->save();

                Toastr::success('Has been add successfully :)','Success');
                DB::commit();
            }
            return redirect()->route('student.list');
        } catch(\Exception $e) {
            Log::info($e);
            DB::rollback();
            Toastr::error('fail, Add new record  :)','Error');
            return redirect()->route('admin.panel');
        }
    }

    public function updateRecordStudent(Request $request, $id)
    {
        DB::beginTransaction();
        try {

            $upload_profile = $request->image_hidden;

            if (!empty($request->upload_image)) {
                $upload_profile = rand() . '.' . $request->upload_image->extension();
                $request->upload_image->move(public_path('/images/avatar/'), $upload_profile);
            }

            $updateRecord = [
                'upload_image' => $upload_profile,
            ];
            Etudiant::where('id',$id)->update($updateRecord);
            Toastr::success('Has been update successfully :)','Success');
            DB::commit();
            return redirect()->route('student.list');

        } catch(\Exception $e) {
            Log::info($e);
            DB::rollback();
            Toastr::error('fail, update record  :)','Error');
            return redirect()->back();
        }
    }

    public function studentDelete($id)
    {
        DB::beginTransaction();
        try {
            Etudiant::destroy($id);
            DB::commit();
            Toastr::success('Deleted record successfully :)','Success');
            return redirect()->back();
        } catch(\Exception $e) {
            DB::rollback();
            Toastr::error('Deleted record fail :)','Error');
            return redirect()->back();
        }
    }

    public function studentInformation()
    {
        $etudiant = Etudiant::where('id',Auth::user()->id)->first();
        return view('',compact('etudiant'));
    }
}
