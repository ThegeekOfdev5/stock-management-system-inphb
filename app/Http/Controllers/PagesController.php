<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PagesController extends Controller
{
    public function AdminPanel(){
        return view('dashboard.admin-panel');
    }

    public function StudentPanel(){
        return view('dashboard.student-panel');
    }

    public function GestionPanel(){
        return view('dashboard.gestion-panel');
    }
}
