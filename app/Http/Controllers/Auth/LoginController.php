<?php

namespace App\Http\Controllers\Auth;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
    * Where to redirect users after login.
    *
    * @var string
    */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('guest')->except([
            'logout',
            'locked',
            'unlock'
        ]);
    }
    /** index page login */
    public function login()
    {
        return view('auth.login');
    }

    /** login with databases */
    public function authenticate(Request $request)
    {
        $request->validate([
            'email'    => 'required|string',
            'password' => 'required|string',
        ]);

        DB::beginTransaction();
        try {

            $email     = $request->email;
            $password  = $request->password;

            if (Auth::attempt(['email'=>$email,'password'=>$password])) {
                /** get session */
                $user = Auth::User();
                Session::put('username', $user->username);
                Session::put('role', $user->role);
                Session::put('email', $user->email);
                Session::put('password', $user->password);
                Session::put('avatar', $user->avatar);
                Session::put('contacts', $user->contacts);
                Session::put('user_id', $user->user_id);
                Session::put('join_date', $user->join_date);
                Session::put('status', $user->status);
                Toastr::success('Login successfully :)','Success');

                if(auth()->user()->role == 'Admin'){
                    return redirect()->route('admin.panel');
                }
                if(auth()->user()->role == 'Etudiant'){
                    return redirect()->route('etudiant.panel');
                }
                if(auth()->user()->role == 'Professeur'){
                    return redirect()->route('professeur.panel');
                }
                if(auth()->user()->role == 'Gestionnaire'){
                    return redirect()->route('gestionnaire.panel');
                }
                if(auth()->user()->role == 'Service'){
                    return redirect()->route('service.panel');
                }
            } else {
                Toastr::error('fail, WRONG USERNAME OR PASSWORD :)','Error');
                return redirect('login');
            }

        } catch(\Exception $e) {
            DB::rollback();
            Toastr::error('fail, LOGIN :)','Error');
            return redirect()->back();
        }
    }

    /** logout */
    public function logout( Request $request)
    {
        Auth::logout();
        // forget login session
        $request->session()->forget('username');
        $request->session()->forget('email');
        $request->session()->forget('user_id');
        $request->session()->forget('join_date');
        $request->session()->forget('contacts');
        $request->session()->forget('status');
        $request->session()->forget('role');
        $request->session()->forget('avatar');
        $request->session()->flush();

        Toastr::success('Logout successfully :)','Success');
        return redirect('login');
    }

}
