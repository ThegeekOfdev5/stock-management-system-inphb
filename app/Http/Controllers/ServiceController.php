<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ServiceController extends Controller
{
    //
    public function serviceEmployeesAdd()
    {
        return view('users.service.add');
    }

    public function serviceEmployeesList()
    {
        $employees = Service::paginate(8);
        return view('users.service.list', compact('employees'));
    }

    public function editRecord($id)
    {
        $employee = Service::where('id',$id)->first();
        return view('users.service.edit',compact('employee'));
    }

    public function saveServiceEmployees(Request $request)
    {
        $request->validate([
            'nom'           => 'required|string',
            'prenom'        => 'required|string',
            'matricule'     => 'required|string',
            'service'       => 'required|string',
            'email'         => 'required|string',
            'contacts'      => 'required|string',
            'upload_image'  => 'required|image',
        ]);

        DB::beginTransaction();

        try {
            $upload_profile = rand() . '.' . $request->upload_image->extension();
            $request->upload_image->move(public_path('/images/avatar/'), $upload_profile);
            if(!empty($request->upload_image)) {

                $dt        = Carbon::now();
                $todayDate = $dt->toDayDateTimeString();


                User::create([
                    'username'  => $request->nom.'@'.$request->service,
                    'email'     => $request->email,
                    'join_date' => $todayDate,
                    'role'      => 'Service',
                    'status'    => 'Active',
                    'contacts'  => $request->contacts,
                    'avatar'    => $upload_profile,
                    'password'  => Hash::make($request->matricule),
                ]);
                $user_id = DB::table('users')->select('user_id')->orderBy('id','DESC')->first();

                $saveRecord = new Service();
                $saveRecord->user_id          = $user_id->user_id;
                $saveRecord->nom              = $request->nom;
                $saveRecord->prenom           = $request->prenom;
                $saveRecord->matricule        = $request->matricule;
                $saveRecord->service          = $request->service;
                $saveRecord->email            = $request->email;
                $saveRecord->contacts         = $request->contacts;
                $saveRecord->upload_image     = $upload_profile;
                $saveRecord->save();

                Toastr::success('Has been add successfully :)','Success');
                DB::commit();
            }
            return redirect()->route('service.list');
        } catch(\Exception $e) {
            Log::info($e);
            DB::rollback();
            Toastr::error('fail, Add new record  :)','Error');
            return redirect()->route('admin.panel');
        }
    }

    public function updateRecordServiceEmployees(Request $request, $id)
    {
        DB::beginTransaction();
        try {

            $upload_profile = $request->image_hidden;

            if (!empty($request->upload_image)) {
                $upload_profile = rand() . '.' . $request->upload_image->extension();
                $request->upload_image->move(public_path('/images/avatar/'), $upload_profile);
            }

            $updateRecord = [
                'nom'          => $request->nom,
                'prenom'       => $request->prenom,
                'matricule'    => $request->matricule,
                'service'      => $request->service,
                'email'        => $request->email,
                'contacts'     => $request->contacts,
                'upload_image' => $upload_profile,
            ];
            Service::where('id',$id)->update($updateRecord);

            Toastr::success('Has been update successfully :)','Success');
            DB::commit();
            return redirect()->route('service.list');

        } catch(\Exception $e) {
            DB::rollback();
            Toastr::error('fail, update record  :)','Error');
            return redirect()->route('service.list');
        }
    }

    public function serviceEmployeesDelete($id)
    {
        DB::beginTransaction();
        try {
            $user = Service::where('id',Auth::user()->id)->first();
            $user->status = 'Disable';
            Service::destroy($id);
            DB::commit();
            Toastr::success('Deleted record successfully :)','Success');
            return redirect()->back();
        } catch(\Exception $e) {
            DB::rollback();
            Toastr::error('Deleted record fail :)','Error');
            return redirect()->back();
        }
    }

    public function serviceEmployeesInformation()
    {
        $employee = Service::where('id',Auth::user()->id)->first();
        return view('',compact('employee'));
    }
}
