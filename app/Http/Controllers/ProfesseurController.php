<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Professeur;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfesseurController extends Controller
{
    //
    public function teacherAdd()
    {
        return view('users.teacher.add');
    }

    public function teacherList()
    {
        $professeurs = Professeur::paginate(8);
        return view('users.teacher.list', compact('professeurs'));
    }

    public function editRecord($id)
    {
        $professeur = Professeur::where('id',$id)->first();
        return view('users.teacher.edit',compact('professeur'));
    }

    public function saveTeacher(Request $request)
    {
        $request->validate([
            'nom'               => 'required|string',
            'prenom'            => 'required|string',
            'matricule'         => 'required|string',
            'type'              => 'required|string',
            'specialite'        => 'required|string',
            'email'             => 'required|string',
            'contacts'          => 'required|string',
            'upload_image'      => 'required|image',
        ]);

        DB::beginTransaction();

        try {
            $upload_profile = rand() . '.' . $request->upload_image->extension();
            $request->upload_image->move(public_path('/images/avatar/'), $upload_profile);
            if(!empty($upload_profile)) {

                $dt        = Carbon::now();
                $todayDate = $dt->toDayDateTimeString();


                User::create([
                    'username'  => $request->nom."@".$request->type,
                    'email'     => $request->email,
                    'join_date' => $todayDate,
                    'role'      => 'Professeur',
                    'status'    => 'Active',
                    'contacts'  => $request->contacts,
                    'avatar'    => $upload_profile,
                    'password'  => Hash::make($request->matricule),
                ]);
                $user_id = DB::table('users')->select('user_id')->orderBy('id','DESC')->first();

                $saveRecord = new Professeur();
                $saveRecord->user_id          = $user_id->user_id;
                $saveRecord->nom              = $request->nom;
                $saveRecord->prenom           = $request->prenom;
                $saveRecord->matricule        = $request->matricule;
                $saveRecord->type             = $request->type;
                $saveRecord->specialite       = $request->specialite;
                $saveRecord->email            = $request->email;
                $saveRecord->contacts         = $request->contacts;
                $saveRecord->upload_image     = $upload_profile;
                $saveRecord->upload_signature = 'NO SIGNATURE';
                $saveRecord->save();

                Toastr::success('Has been add successfully :)','Success');
                DB::commit();
            }
            return redirect()->route('teacher.list');
        } catch(\Exception $e) {
            Log::info($e);
            DB::rollback();
            Toastr::error('fail, Add new record  :)','Error');
            return redirect()->route('admin.panel');
        }
    }

    public function updateRecordTeacher(Request $request,$id)
    {
        DB::beginTransaction();
        try {
            $upload_profile = $request->image_hidden;

            if (!empty($request->upload_image)) {
                $upload_profile = rand() . '.' . $request->upload_image->extension();
                $request->upload_image->move(public_path('/images/avatar/'), $upload_profile);
            }

            $updateRecord = [
                'nom'               => $request->nom,
                'prenom'            => $request->prenom,
                'matricule'         => $request->matricule,
                'type'              => $request->type,
                'specialite'        => $request->specialite,
                'email'             => $request->email,
                'contacts'          => $request->contacts,
                'upload_image'      => $upload_profile,
            ];
            Professeur::where('id',$id)->update($updateRecord);
            Toastr::success('Has been update successfully :)','Success');
            DB::commit();
            return redirect()->route('teacher.list');

        } catch(\Exception $e) {
            DB::rollback();
            Toastr::error('fail, update record  :)','Error');
            return redirect()->route('teacher.list');
        }
    }

    public function teacherDelete($id)
    {
        DB::beginTransaction();
        try {
            $user = Professeur::where('id',Auth::user()->id)->first();
            $user->status = 'Disable';
            Professeur::destroy($id);
            DB::commit();
            Toastr::success('Deleted record successfully :)','Success');
            return redirect()->back();
        } catch(\Exception $e) {
            DB::rollback();
            Toastr::error('Deleted record fail :)','Error');
            return redirect()->back();
        }
    }

    public function teacherInformation()
    {
        $professeur = Professeur::where('id',Auth::user()->id)->first();
        return view('',compact('professeur'));
    }

}
