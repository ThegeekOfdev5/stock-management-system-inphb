<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\DB;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //
    public function index(){
        $utilisateurs = User::paginate(8);
        return view('users.list', compact('utilisateurs'));
    }

    public function overview($id){
        $utilisateur = User::where('id', $id)->first();
        return view('users.overview', compact('utilisateur'));
    }

    public function createAdmin()
    {
        return view('users.admin.create');
    }
    public function storeAdmin(Request $request)
    {
        $request->validate([
            'username'              => 'required|string|max:255',
            'contacts'              => 'required|string|max:255',
            'email'                 => 'required|string|email|max:255|unique:users',
            'password'              => 'required|string|min:8|confirmed',
            'password_confirmation' => 'required',
        ]);

        $dt       = Carbon::now();
        $todayDate = $dt->toDayDateTimeString();

        User::create([
            'username'  => $request->username,
            'email'     => $request->email,
            'contacts'  => $request->contacts,
            'join_date' => $todayDate,
            'role'      => 'Admin',
            'status'    => 'Active',
            'password'  => Hash::make($request->password),
        ]);
        Toastr::success('Create new Admin successfully :)','Success');
        return redirect()->route('admin.users.list');
    }

    public function userUpdate(Request $request, $id){
        DB::beginTransaction();
        try
        {
            $status = $request->status;
            $update = [
                'status'   => $status,
            ];

            User::where('id',$id)->update($update);
            DB::commit();
            Toastr::success('User updated successfully :)','Success');
            return redirect()->route('admin.users.list');

        } catch(\Exception $e){
            DB::rollback();
            Toastr::error('User update fail :)','Error');
            return redirect()->route('admin.users.list');
        }
    }

    public function userDelete($id)
    {
        DB::beginTransaction();
        try {
            User::destroy($id);
            DB::commit();
            Toastr::success('User deleted successfully :)','Success');
            return redirect()->route('admin.users.list');

        } catch(\Exception $e) {
            DB::rollback();
            Toastr::error('User deleted fail :)','Error');
            return redirect()->back();
        }
    }

    /** change password */
    public function changePassword(Request $request)
    {
        $request->validate([
        'current_password'         => ['required'],
            'new_password'         => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);

        User::find(Auth::user()->id)->update(['password'=> Hash::make($request->new_password)]);
        DB::commit();
        Toastr::success('User change successfully :)','Success');
        return redirect()->back();
    }

    public function GetProfil(){
        if(Auth::user()->role == 'Admin'){
            return view('profil.admin');
        }
        if(Auth::user()->role == 'Gestionnaire'){
            return view('profil.gestion');
        }
        if(Auth::user()->role == 'Etudiant'){
            return view('profil.student');
        }
    }

    public function UpdateProfil(Request $request){
        DB::beginTransaction();
        try
        {
            $username       = $request->username;
            $email          = $request->email;
            $contacts       = $request->contacts;
            $upload_profile = $request->image_hidden;
            if (!empty($request->upload_image)) {
                $upload_profile = rand() . '.' . $request->upload_image->extension();
                $request->upload_image->move(public_path('/images/profil/'), $upload_profile);
            }
            $update = [
                'username'   => $username,
                'email'      => $email,
                'contacts'   => $contacts,
                'avatar'     => $upload_profile,
            ];

            User::where('id', Auth::user()->id)->update($update);
            DB::commit();
            Toastr::success('User updated successfully :)','Success');
            return redirect()->back();

        } catch(\Exception $e){
            //dd($e);
            DB::rollback();
            Toastr::error('User update fail :)','Error');
            return redirect()->back();
        }
    }
}
