<?php

namespace App\Http\Controllers\Store;

use App\Models\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class CategorieController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function listCategories()
    {
        $categories = Categories::paginate(8);
        return view('store.category.list', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('store.category.add');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nom'           => 'required|string|max:255',
            'description'   => 'required|string',
            'upload_image'  => 'required|image|file|mimes:jpg,jpeg,png'
        ]);
        try {
            $upload_category = rand() . '.' . $request->upload_image->extension();
            $request->upload_image->move(public_path('/images/categorie/'), $upload_category);
            if(!empty($upload_category)) {

                $saveRecord = new Categories();
                $saveRecord->nom              = $request->nom;
                $saveRecord->description      = $request->description;
                $saveRecord->upload_image     = $upload_category;
                $saveRecord->save();

                Toastr::success('Has been add successfully :)','Success');
                DB::commit();
            }
            return redirect()->route('categorie.list');
        } catch(\Exception $e) {
            Log::info($e);
            DB::rollback();
            Toastr::error('fail, Add new record  :)','Error');
            return redirect()->route('categorie.list');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function editCategorie(string $id)
    {
        $categorie = Categories::where('id',$id)->first();
        return view('store.category.edit', compact('categorie'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function updateCategorie(Request $request, string $id)
    {
        DB::beginTransaction();
        try {

            $upload_category = $request->image_hidden;

            if (!empty($request->upload_image)) {
                $upload_category = rand() . '.' . $request->upload_image->extension();
                $request->upload_image->move(public_path('/images/categorie/'), $upload_category);
            }

            $updateRecord = [
                'nom'           => $request->nom,
                'description'   => $request->description,
                'upload_image'  => $upload_category,
            ];
            Categories::where('id',$id)->update($updateRecord);
            Toastr::success('Has been update successfully :)','Success');
            DB::commit();
            return redirect()->route('categorie.list');

        } catch(\Exception $e) {
            Log::info($e);
            DB::rollback();
            Toastr::error('fail, update record  :)','Error');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function categorieDestroy(string $id)
    {
        DB::beginTransaction();
        try {
            Categories::destroy($id);
            DB::commit();
            Toastr::success('Deleted record successfully :)','Success');
            return redirect()->back();
        } catch(\Exception $e) {
            DB::rollback();
            Toastr::error('Deleted record fail :)','Error');
            return redirect()->back();
        }
    }
}
