<?php

namespace App\Http\Controllers\Store;

use App\Models\Produits;
use App\Models\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class ProduitController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function listProduit()
    {
        $categorie = Categories::all();
        $produits = Produits::paginate(8);
        return view('store.product.list', compact('produits','categorie'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categorie = Categories::all();
        return view('store.product.add', compact('categorie'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'category_id'   => 'required',
            'reference'     => 'required|string|max:255',
            'description'   => 'required|string',
            'quantite'      => 'required|int',
            'emplacement'   => 'required|string',
            'upload_image'  => 'required|image|file|mimes:jpg,jpeg,png'
        ]);
        try {
            $upload_category = rand() . '.' . $request->upload_image->extension();
            $request->upload_image->move(public_path('/images/products/'), $upload_category);
            if(!empty($upload_category)) {

                $saveRecord = new Produits();
                $saveRecord->category_id      = $request->category_id;
                $saveRecord->reference        = $request->reference;
                $saveRecord->description      = $request->description;
                $saveRecord->quantite         = $request->quantite;
                $saveRecord->emplacement      = $request->emplacement;
                $saveRecord->upload_image     = $upload_category;
                $saveRecord->save();

                Toastr::success('Has been add successfully :)','Success');
                DB::commit();
            }
            return redirect()->route('produit.list');
        } catch(\Exception $e) {
            Log::info($e);
            DB::rollback();
            Toastr::error('fail, Add new record  :)','Error');
            return redirect()->route('produit.list');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $produit = Produits::where('id',$id)->first();
        return view('store.product.edit', compact('produit'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        DB::beginTransaction();
        try {

            $upload_produit = $request->image_hidden;

            if (!empty($request->upload_image)) {
                $upload_produit = rand() . '.' . $request->upload_image->extension();
                $request->upload_image->move(public_path('/images/products/'), $upload_produit);
            }

            $updateRecord = [
                'reference'     => $request->reference,
                'description'   => $request->description,
                'quantite'      => $request->quantite,
                'emplacement'   => $request->emplacement,
                'upload_image'  => $upload_produit,
            ];
            Produits::where('id',$id)->update($updateRecord);
            Toastr::success('Has been update successfully :)','Success');
            DB::commit();
            return redirect()->route('produit.list');

        } catch(\Exception $e) {
            Log::info($e);
            DB::rollback();
            Toastr::error('fail, update record  :)','Error');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        DB::beginTransaction();
        try {
            Produits::destroy($id);
            DB::commit();
            Toastr::success('Deleted record successfully :)','Success');
            return redirect()->back();
        } catch(\Exception $e) {
            DB::rollback();
            Toastr::error('Deleted record fail :)','Error');
            return redirect()->back();
        }
    }
}
