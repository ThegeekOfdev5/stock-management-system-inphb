<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\EmployeeGEE;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class EmployeesController extends Controller
{
    //
    public function employeeGEEAdd()
    {
        return view('users.employeegee.add');
    }

    public function employeeGEEList()
    {
        $employees = EmployeeGEE::paginate(8);
        return view('users.employeegee.list', compact('employees'));
    }

    public function editRecord($id)
    {
        $employee = EmployeeGEE::where('id',$id)->first();
        return view('users.employeegee.edit',compact('employee'));
    }

    public function saveEmployeeGEE(Request $request)
    {
        $request->validate([
            'nom'               => 'required|string',
            'prenom'            => 'required|string',
            'matricule'         => 'required|string',
            'poste'             => 'required|string',
            'email'             => 'required|string',
            'contacts'          => 'required|string',
            'upload_image'      => 'required|image',
        ]);

        DB::beginTransaction();

        try {
            $upload_profile = rand() . '.' . $request->upload_image->extension();
            $request->upload_image->move(public_path('/images/avatar/'), $upload_profile);
            if(!empty($upload_profile)) {

                $dt        = Carbon::now();
                $todayDate = $dt->toDayDateTimeString();


                User::create([
                    'username'  => $request->nom.'@'.$request->poste,
                    'email'     => $request->email,
                    'join_date' => $todayDate,
                    'role'      => $request->poste,
                    'status'    => 'Active',
                    'contacts'  => $request->contacts,
                    'avatar'    => $upload_profile,
                    'password'  => Hash::make($request->matricule),
                ]);
                $user_id = DB::table('users')->select('user_id')->orderBy('id','DESC')->first();

                $saveRecord = new EmployeeGEE();
                $saveRecord->user_id          = $user_id->user_id;
                $saveRecord->nom              = $request->nom;
                $saveRecord->prenom           = $request->prenom;
                $saveRecord->matricule        = $request->matricule;
                $saveRecord->poste            = $request->poste;
                $saveRecord->email            = $request->email;
                $saveRecord->contacts         = $request->contacts;
                $saveRecord->upload_image     = $upload_profile;
                $saveRecord->upload_signature = 'NO SIGNATURE';
                $saveRecord->save();

                Toastr::success('Has been add successfully :)','Success');
                DB::commit();
            }
            return redirect()->route('employees.list');
        } catch(\Exception $e) {
            Log::info($e);
            DB::rollback();
            Toastr::error('fail, Add new record  :)','Error');
            return redirect()->route('admin.panel');
        }
    }

    public function updateRecordEmployeeGEE(Request $request, $id)
    {
        DB::beginTransaction();
        try {

            $upload_profile = $request->image_hidden;

            if (!empty($request->upload_image)) {
                $upload_profile = rand() . '.' . $request->upload_image->extension();
                $request->upload_image->move(public_path('/images/avatar/'), $upload_profile);
            }

            $updateRecord = [
                'nom'               => $request->nom,
                'prenom'            => $request->prenom,
                'matricule'         => $request->matricule,
                'poste'             => $request->poste,
                'email'             => $request->email,
                'contacts'          => $request->contacts,
                'upload_image'      => $upload_profile,
            ];
            EmployeeGEE::where('id',$id)->update($updateRecord);

            Toastr::success('Has been update successfully :)','Success');
            DB::commit();
            return redirect()->route('employees.list');

        } catch(\Exception $e) {
            DB::rollback();
            Toastr::error('fail, update record  :)','Error');
            return redirect()->back();
        }
    }

    public function employeeGEEDelete($id)
    {
        DB::beginTransaction();
        try {
            $user = EmployeeGEE::where('id',Auth::user()->id)->first();
            $user->status = 'Disable';
            EmployeeGEE::destroy($id);
            DB::commit();
            Toastr::success('Deleted record successfully :)','Success');
            return redirect()->back();
        } catch(\Exception $e) {
            DB::rollback();
            Toastr::error('Deleted record fail :)','Error');
            return redirect()->back();
        }
    }

    public function employeeGEEInformation()
    {
        $employee = EmployeeGEE::where('id',Auth::user()->id)->first();
        return view('',compact('employee'));
    }
}
