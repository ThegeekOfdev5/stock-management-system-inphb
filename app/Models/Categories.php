<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    use HasFactory;

    protected $fillable = [
        'nom',
        'description',
        'upload_image'
    ];

    public function produits(){
        return $this->belongsTo(Produits::class);
    }
}
