<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('detail_demande', function (Blueprint $table) {
            $table->id();
            $table->foreignId('demandes_id')->constrained('demandes')->cascadeOnDelete();
            $table->foreignId('produits_id')->constrained('produits')->cascadeOnDelete();
            $table->integer('quantite_demande')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('detail_demande');
    }
};
