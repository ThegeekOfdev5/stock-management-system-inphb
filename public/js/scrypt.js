const moon = document.querySelector(".moon")
const sun = document.querySelector(".sun")
const modal = document.getElementById('defaultModal');
//const modalData = document.getElementById('modalContent');
/////////////////////////////////////////////Theme-Modes//////////////////////////////////////////////
//const modeSombreActive = localStorage.getItem('modeSombre') === 'true';
function setDark(val){
    if(val === "dark"){
        document.documentElement.classList.add('dark')
        moon.classList.add("hidden")
        sun.classList.remove("hidden")
        localStorage.setItem('modeSombre', 'true');
    }else{
        document.documentElement.classList.remove('dark')
        sun.classList.add("hidden")
        moon.classList.remove("hidden")
        localStorage.setItem('modeSombre', 'false');
    }
}

window.onload = function() {
    const modeSombre = localStorage.getItem('modeSombre');
    if (modeSombre === 'true') {
        setDark('dark');
    } else {
        setDark('light');
    }
}
