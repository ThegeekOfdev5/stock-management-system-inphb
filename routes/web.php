<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Request;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\EtudiantController;
use App\Http\Controllers\EmployeesController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\ProfesseurController;
use App\Http\Controllers\Store\CategorieController;
use App\Http\Controllers\Store\ProduitController;
use App\Http\Controllers\Store\StoreController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
/** for side bar menu active */
function set_active( $route ): string
{
    if( is_array( $route ) ){
        return in_array(Request::path(), $route) ? 'bg-gray-200' : 'bg-transparent';
    }
    return Request::path() == $route ? 'bg-gray-200' : 'bg-transparent';
}


Auth::routes();

//----------------------------store ------------------------------//
Route::controller(StoreController::class)->group(function () {
    Route::get('/', 'index')->name('store');
});
//----------------------------login ------------------------------//
Route::controller(LoginController::class)->group(function () {
    Route::get('/login', 'login')->name('login');
    Route::post('/login', 'authenticate');
    Route::get('/logout', 'logout')->name('logout');
    Route::post('change/password', 'changePassword')->name('change/password');
});
// ----------------------------Admin page access ------------------------------//
Route::middleware(['auth', 'user-access:Admin'])->group(function(){
    Route::get('/admin/panel', [PagesController::class, 'AdminPanel'])->name('admin.panel');
    Route::controller(UserController::class)->prefix('admin-management')->group(function(){
        Route::get('/create/page', 'createAdmin')->name('admin.add.page');
        Route::post('/create/save', 'storeAdmin')->name('admin.add.save');
        Route::get('/user-list', 'index')->name('admin.users.list');
        Route::get('/edit/{id}', 'overview')->name('admin.users.edit');
        Route::post('/update/{id}', 'userUpdate')->name('admin.users.update');
        Route::get('/delete/{id}', 'userDelete')->name('admin.users.delete');
    })->name('admin-management');

    Route::controller(EtudiantController::class)->prefix('student-management')->group(function(){
        Route::get('/list', 'studentList')->name('student.list'); // list student
        Route::get('/add/page', 'studentAdd')->name('student.add.page'); // page student
        Route::post('/add/save', 'saveStudent')->name('student.add.store'); // save record student
        Route::get('/edit/{id}', 'editRecord')->name('student.edit.page'); // view for edit
        Route::post('/update/{id}', 'updateRecordStudent')->name('student.update'); // update record student
        Route::get('/delete/{id}', 'studentDelete')->name('student.delete'); // delete record student
    })->name('student-management');

    Route::controller(ProfesseurController::class)->prefix('teacher-management')->group(function(){
        Route::get('/list', 'teacherList')->name('teacher.list'); // list teacher
        Route::get('/add/page', 'teacherAdd')->name('teacher.add.page'); // page teacher
        Route::post('/add/save', 'saveTeacher')->name('teacher.add.store'); // save record teacher
        Route::get('/edit/{id}', 'editRecord')->name('teacher.edit.page'); // view for edit
        Route::post('/update/{id}', 'updateRecordTeacher')->name('teacher.update'); // update record teacher
        Route::get('/delete/{id}', 'teacherDelete')->name('teacher.delete'); // delete record student
    })->name('teacher-management');

    Route::controller(EmployeesController::class)->prefix('employees-management')->group(function(){
        Route::get('/list', 'employeeGEEList')->name('employees.list'); // list employees
        Route::get('/add/page', 'employeeGEEAdd')->name('employees.add.page'); // page employees
        Route::post('/add/save', 'saveEmployeeGEE')->name('employees.add.store'); // save record employees
        Route::get('/edit/{id}', 'editRecord')->name('employees.edit.page'); // view for edit
        Route::post('/update/{id}', 'updateRecordEmployeeGEE')->name('employees.update'); // update record employees
        Route::get('/delete/{id}', 'employeeGEEDelete')->name('employees.delete'); // delete record student
    })->name('employees-management');

    Route::controller(ServiceController::class)->prefix('service-management')->group(function(){
        Route::get('/list', 'serviceEmployeesList')->name('service.list'); // list service
        Route::get('/add/page', 'serviceEmployeesAdd')->name('service.add.page'); // page service
        Route::post('/add/save', 'saveServiceEmployees')->name('service.add.store'); // save record service
        Route::get('/edit/{id}', 'editRecord')->name('service.edit.page'); // view for edit
        Route::post('/update/{id}', 'updateRecordServiceEmployees')->name('service.update'); // update record service
        Route::get('/delete/{id}', 'serviceEmployeesDelete')->name('service.delete'); // delete record student
    })->name('service-management');
});
// ----------------------------Etudiant page access ------------------------------//
Route::middleware(['auth', 'user-access:Etudiant'])->group(function(){
    Route::get('/etudiant/panel', [PagesController::class, 'StudentPanel'])->name('etudiant.panel');
});
// ----------------------------Gestionnaire page access ------------------------------//
Route::middleware(['auth', 'user-access:Gestionnaire'])->group(function(){
    Route::get('/gestionnaire/panel', [PagesController::class, 'GestionPanel'])->name('gestionnaire.panel');

    Route::controller(ProduitController::class)->prefix('produit-management')->group(function(){
        Route::get('/list', 'listProduit')->name('produit.list'); // list produit
        Route::get('/add/page', 'create')->name('produit.add.page'); // page produit
        Route::post('/add/save', 'store')->name('produit.add.store'); // save record produit
        Route::get('/edit/{id}', 'edit')->name('produit.edit.page'); // view for edit
        Route::post('/update/{id}', 'update')->name('produit.update'); // update record produit
        Route::get('/delete/{id}', 'destroy')->name('produit.delete'); // delete record produit
    })->name('produit-management');

    Route::controller(CategorieController::class)->prefix('categorie-management')->group(function(){
        Route::get('/list', 'listCategories')->name('categorie.list'); // list categorie
        Route::get('/add/page', 'create')->name('categorie.add.page'); // page categorie
        Route::post('/add/save', 'store')->name('categorie.add.store'); // save record categorie
        Route::get('/edit/{id}', 'editCategorie')->name('categorie.edit.page'); // view for edit
        Route::post('/update/{id}', 'updateCategorie')->name('categorie.update'); // update record categorie
        Route::get('/delete/{id}', 'categorieDestroy')->name('categorie.delete'); // delete record student
    })->name('categorie-management');
});




Route::middleware(['auth'])->group(function(){
    Route::get('/store', [ProduitController::class, 'indexStore'])->name('store');
    Route::get('/profil', [UserController::class, 'GetProfil'])->name('admin.profil');
    Route::post('/change-password',[UserController::class, 'changePassword'])->name('change.password');
    Route::post('/update-profil',[UserController::class, 'UpdateProfil'])->name('update.profil');
});

/*Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});
*/

Route::get('/test', function(){
    return view('errors.500');
});

require __DIR__.'/auth.php';
