@extends('templates.gestion')
@section('content')
<nav class = "flex px-5 py-3 text-gray-700 mb-3 rounded-lg bg-gray-50 dark:bg-[#1E293B] " aria-label="Breadcrumb">
    <ol class = "inline-flex items-center space-x-1 md:space-x-3">
        <li class = "inline-flex items-center">
            <a href="{{ route('gestionnaire.panel') }}" class = "inline-flex items-center text-sm font-medium text-gray-700 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white">
                <svg class = "w-4 h-4 mr-2" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z"></path></svg>
                Home
            </a>
        </li>
        <li class = "inline-flex items-center">
            <a href="{{ route('produit.list') }}" class = "inline-flex items-center text-sm font-medium text-gray-700 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white">
                <svg class = "w-6 h-6 text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clipRule="evenodd"></path></svg>
                Liste des materiaux
            </a>
        </li>
        <li>
            <div class = "flex items-center">
                <svg class = "w-6 h-6 text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clipRule="evenodd"></path></svg>
                <p href="#" class = "ml-1 text-sm font-medium text-gray-700 hover:text-gray-900 md:ml-2 dark:text-gray-400 dark:hover:text-white">Ajouter un materiel</p>
            </div>
        </li>
    </ol>
</nav>
<div class="bg-white border-4 rounded-lg shadow relative mx-10 mt-0">
    {{-- message --}}
    {!! Toastr::message() !!}
    <div class="p-4 space-y-4">
        <form action="{{ route('produit.add.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="grid grid-cols-6 gap-4">
                <div class="col-span-6 sm:col-span-3">
                    <label for="nom" class="text-sm font-medium text-gray-900 block mb-1">Categorie</label>
                    <select class="@error('categorie') is-invalid @enderror shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-1" name="category_id">
                        <option>Select Categorie</option>
                        @foreach ($categorie as $type)
                            <option value="{{ $type->id }}">{{ $type->nom }}</option>
                        @endforeach
                    </select>
                    @error('categorie')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-span-6 sm:col-span-3">
                    <label for="nom" class="text-sm font-medium text-gray-900 block mb-1">Reference</label>
                    <input type="text" name="reference" id="product-name" class=" @error('reference') is-invalid @enderror shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-1" value="{{ old('reference') }}">
                    @error('reference')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-span-6 sm:col-span-3">
                    <label for="nom" class="text-sm font-medium text-gray-900 block mb-1">Quantité</label>
                    <input type="number" name="quantite" id="product-name" class=" @error('quantite') is-invalid @enderror shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-1" value="{{ old('quantite') }}">
                    @error('quantite')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-span-6 sm:col-span-3">
                    <label for="nom" class="text-sm font-medium text-gray-900 block mb-1">Emplacement</label>
                    <input type="text" name="emplacement" placeholder="ex: 01|01|02|05 si Electronique" id="product-name" class=" @error('emplacement') is-invalid @enderror shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-1" value="{{ old('emplacement') }}">
                    @error('emplacement')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-span-6 sm:col-span-3">
                    <label for="upload" class="text-sm font-medium text-gray-900 block mb-1">Images</label>
                    <input type="file" name="upload_image" id="price" class="@error('upload_image') is-invalid @enderror block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50 dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400" value="{{ old('upload_image') }}">
                    @error('upload_image')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-span-full">
                    <label for="description" class="text-sm font-medium text-gray-900 block mb-1">Description</label>
                    <textarea name="description" id="description" rows="6" class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-4" placeholder="Details" value="{{ old('description') }}"></textarea>
                    @error('description')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="p-1 mt-8 border-t border-gray-200 rounded-b">
                <button class="text-white bg-slate-400 hover:bg-slate-200 focus:ring-4 focus:ring-slate-200 font-medium rounded-lg text-sm px-5 py-2.5 text-center" type="submit">Sauvegader</button>
            </div>
        </form>
    </div>
</div>
@endsection
