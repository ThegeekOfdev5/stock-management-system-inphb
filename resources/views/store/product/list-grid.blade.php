@extends('templates.store')
@section('content')
<div class="bg-white">
    <form class="flex flex-col md:flex-row gap-3 px-36 justify-end">
        <div class="flex">
            <input type="text" placeholder="Search for the tool you like"
                class="w-full md:w-80 px-3 h-10 rounded-l border-2 border-slate-500 focus:outline-none focus:border-slate-500"
                >
            <button type="submit" class="bg-slate-400 text-white rounded-r px-2 md:px-3 py-0 md:py-1">Recherche</button>
        </div>
        <select id="pricingType" name="pricingType"
            class="w-32 h-10 border-2 border-slate-500 focus:outline-none focus:border-slate-500 text-slate-500 rounded px-2 md:px-3 py-0 md:py-1 tracking-wider">
            <option value="All" selected="">All</option>
            <option value="Freemium">Freemium</option>
            <option value="Free">Free</option>
            <option value="Paid">Paid</option>
        </select>
    </form>
    <div class="mx-32 max-w-2xl px-4 py-2 sm:px-6 sm:py-2 lg:max-w-7xl lg:px-8">
        <h2 class="text-4xl font-bold tracking-tight mb-4 text-gray-900">Produits</h2>
        <div class="mt-4 grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
            <div class="group relative bg-gray-300 rounded-md">
                <div class="aspect-h-1 aspect-w-1 w-full overflow-hidden bg-gray-200 lg:aspect-none group-hover:opacity-75 lg:h-80">
                    <img src="{{ URL::to('images/products/s7.jpg') }}" alt="Front of men&#039;s Basic Tee in black." class="h-full w-full object-cover object-center lg:h-full lg:w-full">
                </div>
                <div class="mt-4 flex justify-between p-1">
                    <div>
                        <h3 class="text-sm text-gray-700">
                            <a href="#">
                            <span aria-hidden="true" class="absolute inset-0"></span>
                            Produit name
                            </a>
                        </h3>
                        <p class="mt-1 text-sm text-gray-500">status</p>
                    </div>
                    <p class="text-sm font-medium text-gray-900">35</p>
                </div>
            </div>
        <!-- More products... -->
        </div>
    </div>
</div>
@endsection
