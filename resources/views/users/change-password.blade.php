<div class="bg-white shadow rounded-lg p-2 mt-2">
    <h2 class="text-xl font-bold mb-4">Modifier mon mot de passe</h2>
    <form action="{{ route('change.password') }}" method="POST" class="grid grid-cols-6 gap-4">
        @csrf
        <div class="col-span-6 sm:col-span-3">
            <label for="username" class="text-sm font-medium text-gray-900 block mb-2">Ancient mot de passe</label>
            <input type="text" name="current_password" id="product-name"  class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-2">
        </div>
        <div class="col-span-6 sm:col-span-3">
            <label for="email" class="text-sm font-medium text-gray-900 block mb-2">Nouveau mot de passe</label>
            <input type="text" name="new_password" id="product-name"  class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-2">
        </div>
        <div class="col-span-6 sm:col-span-3">
            <label for="role" class="text-sm font-medium text-gray-900 block mb-2">Confirmer le nouveau mot de passe</label>
            <input type="text" name="new_confirm_password" id="product-name"  class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-2">
        </div>
        <div class="col-span-6 p-1 mt-6 sm:col-span-3">
            <button class="text-white bg-slate-400 hover:bg-slate-200 focus:ring-4 focus:ring-slate-200 font-medium rounded-lg text-sm px-5 py-2.5 text-center" type="submit">Sauvegader</button>
        </div>
    </form>
</div>
