@extends('templates.store')
@section('content')
<div class="bg-white mx-48 mb-24">
    <div class="px-2 py-10 w-full flex justify-center">
        <div class="bg-white lg:mx-8 lg:flex lg:max-w-5xl lg:shadow-lg rounded-lg">
            <div class="lg:w-1/2">
                <div class="lg:scale-110 h-32 bg-cover lg:h-full rounded-b-none border lg:rounded-lg"
                    style="background-image:url('https://images.unsplash.com/photo-1517694712202-14dd9538aa97')">
                </div>
            </div>
            <div class="py-12 px-6 lg:px-12 max-w-xl lg:max-w-5xl lg:w-1/2 rounded-t-none border lg:rounded-lg">
                <h2 class="text-4xl text-gray-800 font-bold">
                    Produit name
                </h2>
                <h2 class="text-sm text-gray-800 font-bold">
                    Designation
                </h2>
                <p class="mt-4 text-gray-600">
                    The "Eco-Tracker" project aims to create a web-based platform that encourages individuals to adopt
                    sustainable lifestyle choices and actively contribute to environmental conservation. The platform will...
                </p>
                <h2 class="text-sm text-gray-800 font-bold">
                    Quantite en stock
                </h2>
                <h2 class="text-xl text-gray-800 font-bold">
                    100
                </h2>
                <div class="mt-8">
                    <a href="#" class="bg-gray-900 text-gray-100 px-5 py-3 font-semibold rounded">Ajouter au panier</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection