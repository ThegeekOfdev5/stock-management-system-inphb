@extends('templates.admin')
@section('content')
{!! Toastr::message() !!}
    <div class="p-1">
        <nav class = "flex px-5 py-3 text-gray-700  rounded-lg bg-gray-50 dark:bg-[#1E293B] " aria-label="Breadcrumb">
            <ol class = "inline-flex items-center space-x-1 md:space-x-3">
                <li class = "inline-flex items-center">
                    <a href="{{ route('admin.panel') }}" class = "inline-flex items-center text-sm font-medium text-gray-700 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white">
                        <svg class = "w-4 h-4 mr-2" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z"></path></svg>
                        Home
                    </a>
                </li>
                <li>
                    <div class = "flex items-center">
                        <svg class = "w-6 h-6 text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clipRule="evenodd"></path></svg>
                        <p href="#" class = "ml-1 text-sm font-medium text-gray-700 hover:text-gray-900 md:ml-2 dark:text-gray-400 dark:hover:text-white">Liste des employées du GEE</p>
                    </div>
                </li>
            </ol>
        </nav>
        <div class="flex justify-end mt-4">
            <a href="{{ route('employees.add.page') }}" class="focus:outline-none text-white bg-yellow-400 hover:bg-yellow-500 focus:ring-4 focus:ring-yellow-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:focus:ring-yellow-900">Ajouter un employée</a>
        </div>
        <div class="relative overflow-x-auto mt-4 sm:rounded-lg">
            <div class="pb-4 dark:bg-gray-900">
                <label for="table-search" class="sr-only">Search</label>
                <div class="relative mt-1">
                    <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                        <svg class="w-4 h-4 text-gray-500 dark:text-gray-400" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"/>
                        </svg>
                    </div>
                    <input type="text" id="table-search" class="block p-2 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg w-80 bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Search for items">
                </div>
            </div>
            <table class="w-full divide-y divide-gray-200">
                <thead class="bg-gray-50 dark:bg-[#1E293B]">
                    <tr>
                        <th scope="col" class="px-2 py-1 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Matricule
                        </th>
                        <th scope="col" class="px-2 py-1 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Nom et Prenom
                        </th>
                        <th scope="col" class="px-2 py-1 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Poste
                        </th>
                        <th scope="col" class="px-2 py-1 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Contacts
                        </th>
                        <th scope="col" class="px-2 py-1 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Actions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($employees as $key => $list)

                    <tr class="bg-white divide-y divide-gray-200 dark:bg-[#1E293B]">
                        <td class="px-2 py-1 whitespace-nowrap text-sm text-gray-500">
                            {{ $list->matricule}}
                        </td>
                        <td class="px-2 py-1 whitespace-nowrap">
                            <div class="flex items-center">
                                <div class="flex-shrink-0 h-10 w-10">
                                    <img class="h-10 w-10 rounded-full" src="/images/avatar/{{ $list->upload_image }}" alt="">
                                </div>
                                <div class="ml-2">
                                    <div class="text-sm font-medium text-gray-900 dark:text-gray-500">
                                        {{ $list->nom .'  '.$list->prenom}}
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="px-2 py-1 whitespace-nowrap text-sm text-gray-500">
                            {{ $list->poste }}
                        </td>
                        <td class="px-2 py-1 whitespace-nowrap text-sm text-gray-500">
                            {{ $list->contacts }}
                        </td>
                        <td class="px-2 py-1 whitespace-nowrap  text-sm font-medium">
                            <a href="{{ url('employees-management/edit/'.$list->id)}}" class="text-indigo-600 hover:text-indigo-900">Edit</a>
                            <a href="{{ url('employees-management/delete/'.$list->id)}}" class="ml-2 text-red-600 hover:text-red-900">Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="h-8 text-sm mt-2">
            {{ $employees->links() }}
        </div>
    </div>
@endsection
