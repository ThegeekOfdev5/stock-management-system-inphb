{{-- message --}}
{!! Toastr::message() !!}
<div class=" bg-gray-100 dark:bg-gray-900">
    <div class="container mx-auto bg-gray-100 dark:bg-gray-900">
        <div class="grid grid-cols-4 sm:grid-cols-12 gap-6 px-4">
            <div class="col-span-4 sm:col-span-3">
                <div class="bg-white shadow rounded-lg p-4">
                    <div class="flex flex-col items-center">
                        <img src="/images/avatar/{{ Auth::user()->avatar }}" class="w-32 h-32 bg-gray-300 rounded-full mb-4 shrink-0">
                        <h1 class="text-xl font-bold">{{ Auth::user()->username }}</h1>
                        <p class="text-gray-600">{{ Auth::user()->role}}</p>
                    </div>
                </div>
            </div>
            <div class="col-span-4 sm:col-span-9 bg-gray-100 dark:bg-gray-900">
                <div class="bg-white shadow rounded-lg p-6">
                    <h2 class="text-xl font-bold mb-4">Modifier mon profil</h2>
                    <form action="{{ route('update.profil') }}" method="POST" class="grid grid-cols-6 gap-4" enctype="multipart/form-data">
                        @csrf
                        <div class="col-span-6 sm:col-span-3">
                            <label for="username" class="text-sm font-medium text-gray-900 block mb-2">Nom d'utilisateur</label>
                            <input type="text" name="username" id="product-name" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-2" value="{{ Auth::user()->username}}">
                        </div>
                        <div class="col-span-6 sm:col-span-3">
                            <label for="email" class="text-sm font-medium text-gray-900 block mb-2">Email</label>
                            <input type="text" name="email" id="product-name" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-2" value="{{ Auth::user()->email }}">
                        </div>
                        <div class="col-span-6 sm:col-span-3">
                            <label for="role" class="text-sm font-medium text-gray-900 block mb-2">Role</label>
                            <input type="text" name="role" id="product-name" disabled class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-2" value="{{ Auth::user()->role }}">
                        </div>
                        <div class="col-span-6 sm:col-span-3">
                            <label for="contacts" class="text-sm font-medium text-gray-900 block mb-2">Contacts</label>
                            <input type="text" name="contacts" id="product-name" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-2" value="{{ Auth::user()->contacts }}">
                        </div>
                        <div class="col-span-6 sm:col-span-3">
                            <label for="nom" class="text-sm font-medium text-gray-900 block mb-2">Image</label>
                            <input type="file" name="upload_image" id="price" class="@error('upload_image') is-invalid @enderror block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50 dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400">
                            @error('upload_image')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <input type="hidden" name="image_hidden" value="{{ Auth::user()->avatar }}">
                        </div>
                        <div class="col-span-6 p-1 mt-6 sm:col-span-3">
                            <button class="text-white bg-slate-400 hover:bg-slate-200 focus:ring-4 focus:ring-slate-200 font-medium rounded-lg text-sm px-5 py-2.5 text-center" type="submit">Sauvegader</button>
                        </div>
                    </form>
                </div>
                @include('users.change-password')
            </div>
        </div>
    </div>
</div>
