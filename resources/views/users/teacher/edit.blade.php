@extends('templates.admin')
@section('content')
<nav class = "flex px-5 py-3 text-gray-700 mb-3 rounded-lg bg-gray-50 dark:bg-[#1E293B] " aria-label="Breadcrumb">
    <ol class = "inline-flex items-center space-x-1 md:space-x-3">
        <li class = "inline-flex items-center">
            <a href="{{ route('admin.panel') }}" class = "inline-flex items-center text-sm font-medium text-gray-700 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white">
                <svg class = "w-4 h-4 mr-2" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z"></path></svg>
                Home
            </a>
        </li>
        <li class = "inline-flex items-center">
            <a href="{{ route('teacher.list') }}" class = "inline-flex items-center text-sm font-medium text-gray-700 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white">
                <svg class = "w-6 h-6 text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clipRule="evenodd"></path></svg>
                Liste des Employés du GEE
            </a>
        </li>
        <li>
            <div class = "flex items-center">
                <svg class = "w-6 h-6 text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clipRule="evenodd"></path></svg>
                <p href="#" class = "ml-1 text-sm font-medium text-gray-700 hover:text-gray-900 md:ml-2 dark:text-gray-400 dark:hover:text-white">Modifier un employé</p>
            </div>
        </li>
    </ol>
</nav>
<div class="bg-white border-4 rounded-lg shadow relative mx-10 mt-0">
    {{-- message --}}
    {!! Toastr::message() !!}
    <div class="p-4 space-y-4">
        <form action="{{ url('teacher-management/update/'.$professeur->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="grid grid-cols-6 gap-4">
                <div class="col-span-6 sm:col-span-3">
                    <label for="nom" class="text-sm font-medium text-gray-900 block mb-2">Nom</label>
                    <input type="text" name="nom" id="product-name" class=" @error('nom') is-invalid @enderror shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-2" value="{{$professeur->nom }}">
                    @error('nom')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-span-6 sm:col-span-3">
                    <div class="col-span-6 sm:col-span-3">
                        <label for="prenom" class="text-sm font-medium text-gray-900 block mb-2">Prenom</label>
                        <input type="text" name="prenom" id="price" class="@error('prenom') is-invalid @enderror shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-2" value="{{$professeur->prenom}}">
                        @error('prenom')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-span-6 sm:col-span-3">
                    <label for="matricule" class="text-sm font-medium text-gray-900 block mb-2">Matricule</label>
                    <input type="text" name="matricule" id="brand" class="@error('matricule') is-invalid @enderror shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-2" value="{{$professeur->matricule}}">
                    @error('matricule')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
                <div class="col-span-6 sm:col-span-3">
                    <label for="ecole" class="text-sm font-medium text-gray-900 block mb-2">Qualificatif</label>
                    <input type="text" name="type" id="price" class="@error('type') is-invalid @enderror shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-2" value="{{$professeur->type}}">
                    @error('type')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
                <div class="col-span-6 sm:col-span-3">
                    <label for="parcours" class="text-sm font-medium text-gray-900 block mb-2">Departement ou Matière</label>
                    <input type="text" name="specialite" id="brand" class="@error('specialite') is-invalid @enderror shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-2" value="{{$professeur->specialite}}">
                    @error('specialite')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
                <div class="col-span-6 sm:col-span-3">
                    <label for="email" class="text-sm font-medium text-gray-900 block mb-2">Email</label>
                    <input type="email" name="email" id="price" class="@error('email') is-invalid @enderror shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-2" value="{{$professeur->email}}">
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
                <div class="col-span-6 sm:col-span-3">
                    <label for="contacts" class="text-sm font-medium text-gray-900 block mb-2">Contacts</label>
                    <input type="text" name="contacts" id="brand" class="@error('contacts') is-invalid @enderror shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-2" value="{{ $professeur->contacts}}">
                    @error('contacts')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
                <div class="col-span-6 sm:col-span-3 flex">
                    <div class="flex mr-2">
                        <img class="h-10 w-10 rounded-full" src="/images/profil/{{ $professeur->upload_image }}" alt="User Image">
                        <input type="hidden" name="image_hidden" value="{{ $professeur->upload_image }}">
                    </div>
                    <label for="upload" class="@error('upload_image') is-invalid @enderror h-6 text-sm text-gray-900">
                        <input type="file" name="upload_image" class="font-thin">
                    </label>
                    @error('upload_image')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="p-1 mt-3 border-t border-gray-200 rounded-b">
                <button class="text-white bg-slate-400 hover:bg-slate-200 focus:ring-4 focus:ring-slate-200 font-medium rounded-lg text-sm px-5 py-2.5 text-center" type="submit">Sauvegader</button>
            </div>
        </form>
    </div>
</div>
@endsection
