@extends('templates.admin')
@section('content')
<nav class = "flex px-5 py-3 text-gray-700 mb-10 rounded-lg bg-gray-50 dark:bg-[#1E293B] " aria-label="Breadcrumb">
    <ol class = "inline-flex items-center space-x-1 md:space-x-3">
        <li class = "inline-flex items-center">
            <a href="{{ route('admin.panel') }}" class = "inline-flex items-center text-sm font-medium text-gray-700 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white">
                <svg class = "w-4 h-4 mr-2" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z"></path></svg>
                Home
            </a>
        </li>
        <li class = "inline-flex items-center">
            <a href="{{ route('admin.users.list') }}" class = "inline-flex items-center text-sm font-medium text-gray-700 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white">
                <svg class = "w-6 h-6 text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clipRule="evenodd"></path></svg>
                Liste des utilisateurs
            </a>
        </li>
        <li>
            <div class = "flex items-center">
                <svg class = "w-6 h-6 text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clipRule="evenodd"></path></svg>
                <p href="#" class = "ml-1 text-sm font-medium text-gray-700 hover:text-gray-900 md:ml-2 dark:text-gray-400 dark:hover:text-white">Creer un administrateur</p>
            </div>
        </li>
    </ol>
</nav>
<div class="bg-white border-4 rounded-lg shadow relative mx-10 mt-0">
    {!! Toastr::message() !!}
    <div class="p-6 space-y-6">
        <form action="{{ route('admin.add.save') }}" method="POST">
            @csrf
            <div class="grid grid-cols-6 gap-4">
                <div class="col-span-6 sm:col-span-3">
                    <label for="nom" class="text-sm font-medium text-gray-900 block mb-2">Nom d'utilisateur</label>
                    <input type="text" name="username" id="product-name" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-2.5" value="{{ old('username') }}">
                </div>
                <div class="col-span-6 sm:col-span-3">
                    <div class="col-span-6 sm:col-span-3">
                        <label for="prenom" class="text-sm font-medium text-gray-900 block mb-2">Email</label>
                        <input type="email" name="email" id="price" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-2.5" value="{{ old('email') }}">
                    </div>
                </div>
                <div class="col-span-6 sm:col-span-3">
                    <label for="matricule" class="text-sm font-medium text-gray-900 block mb-2">Mot de passe</label>
                    <input type="text" name="password" id="brand" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-2.5" value="{{ old('password') }}">
                </div>
                <div class="col-span-6 sm:col-span-3">
                    <label for="ecole" class="text-sm font-medium text-gray-900 block mb-2">Confirmation du mot de passe</label>
                    <input type="text" name="password_confirmation" id="price" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-2.5" value="{{ old('password_confirmation') }}">
                </div>
                <div class="col-span-6 sm:col-span-3">
                    <label for="matricule" class="text-sm font-medium text-gray-900 block mb-2">Contacts</label>
                    <input type="text" name="contacts" id="brand" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-2.5" value="{{ old('password') }}">
                </div>
            </div>
            <div class="p-2 mt-8 border-t border-gray-200 rounded-b">
                <button class="text-white bg-slate-400 hover:bg-slate-200 focus:ring-4 focus:ring-slate-200 font-medium rounded-lg text-sm px-5 py-2.5 text-center" type="submit">Sauvegader</button>
            </div>
        </form>
    </div>
</div>
@endsection
